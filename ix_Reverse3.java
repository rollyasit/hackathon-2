import java.util.Arrays;
import java.util.Scanner;

public class ix_Reverse3 {

	public static void main(String[] args) {
		int[] arr= {3,2,4,7,0,3,1,5,8,4};
		Scanner sc=new Scanner(System.in);
		System.out.println("Please enter how many no in array you want to reverse.");
		int no=sc.nextInt();
		int Start=0;
		int End=0;
		int temp=0;
		int num=arr.length-(arr.length/no);
		for (int i=0; i<num; i=i+no)
		{
			Start=i;
			End=i+no-1;
			while(Start<=End) 
			{
				temp=arr[Start];
				arr[Start]=arr[End];
				arr[End]=temp;
				Start++;
				End--;
			}
		}
		System.out.println(Arrays.toString(arr));
	}

}
