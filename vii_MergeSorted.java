import java.util.Arrays;

public class vii_MergeSorted {

	public static void main(String[] args) {
		 
		int[] arr2= {1,5,7,8};
		int arr1[]=new int[10];
		arr1[0]=2;
		arr1[1]=3;
		arr1[2]=4;
		arr1[3]=6;
		arr1[4]=9;
		arr1[5]=10;
		
		int arr1No=6, arr2No=4;
		int i=arr1No-1, j=arr2No-1, index=arr1No+arr2No-1;
				
		while(i>=0 && j>=0) 
		{
			if (arr1[i]>arr2[j]) 
			{
				arr1[index]=arr1[i];
				
				i=i-1;
			}
			else
			{
				arr1[index]=arr2[j];
				j=j-1;
			}
			index=index-1;
		}
        while (i >= 0) {
        	arr1[index--] = arr1[i--];
        }
        while (j >= 0) {
        	arr1[index--] = arr2[j--];
        }
		System.out.println(Arrays.toString(arr1));

	}

}
