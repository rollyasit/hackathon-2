import java.util.Collections;
import java.util.LinkedList;

public class iii_RankStudent {

	public static void main(String[] args) {
		ii_Student st1=new ii_Student("Rolly", 111,100);
		ii_Student st2=new ii_Student("Aanchal", 222,60);
		ii_Student st3=new ii_Student("Asit", 333,70);
		ii_Student st4=new ii_Student("Aarav", 444,40);
		ii_Student st5=new ii_Student("Aavya", 555,30);
		
		LinkedList<ii_Student> stu=new LinkedList<ii_Student>();
		stu.add(st1);
		stu.add(st2);
		stu.add(st3);
		stu.add(st4);
		stu.add(st5);
		
		Collections.sort(stu);
		for(int i=0; i<stu.size();i++)
		{
			System.out.println(stu.get(i).stuName +"'s rank is "+ (i+1));
		}
	}

}
