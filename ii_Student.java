
public class ii_Student implements Comparable<ii_Student> 
{
	int StuRegNo=0;
	int StuMarks=0;
	String stuName="";	
	
	public ii_Student(String stuName, int StuRegNo, int StuMarks)
	{
		this.StuRegNo=StuRegNo;
		this.stuName=stuName;
		this.StuMarks=StuMarks;
	}

	@Override
	public int compareTo(ii_Student c) {
		
		return c.StuMarks-this.StuMarks;
	}


}
