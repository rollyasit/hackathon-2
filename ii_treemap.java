import java.util.Iterator;
import java.util.TreeMap;

public class ii_treemap {

	public static void main(String[] args) {
		TreeMap<Integer, ii_Student> stu=new TreeMap<Integer, ii_Student>();
		
		ii_Student st1=new ii_Student("Rolly", 111,100);
		ii_Student st2=new ii_Student("Aanchal", 222,80);
		ii_Student st3=new ii_Student("Asit", 333,60);
		ii_Student st4=new ii_Student("Aarav", 444,40);
		ii_Student st5=new ii_Student("Aavya", 555,10);
		stu.put(1, st1);
		stu.put(2, st2);
		stu.put(3, st3);
		stu.put(4, st4);
		stu.put(5, st5);
		

		for(int i=1; i<=stu.size();i++)
		{
			System.out.println(stu.get(i).stuName +" - " + stu.get(i).StuRegNo);
		}

		
		
	}

}
