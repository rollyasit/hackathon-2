import java.util.Scanner;

public class v_Cent {
	public static void main(String[] args)
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Please enter total no of cents.");
		int num=sc.nextInt();
		
		int dollar;
		int quarter;
		int dime;
		int pennies;
		dollar=num/100;
		quarter=(num%100)/25;
		dime=((num%100)%25)/10;
		pennies=(((num%100)%25)%10);
		System.out.println("Total no of coins are: " + (dollar+quarter+dime+pennies));
	}
}
